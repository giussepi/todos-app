# -*- coding: utf-8 -*-
""" spa's models """

from django.db import models


class Todo(models.Model):
    """ stores the todo items """
    title = models.CharField(max_length=120)
    completed = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % self.title
