# -*- coding: utf-8 -*-
""" spa's urls """

from django.conf.urls import patterns, url  #,include

from .views import IndexView

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='spa.index'),
)
