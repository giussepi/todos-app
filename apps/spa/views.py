# -*- coding: utf-8 -*-
""" spa's view """

from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = 'spa/index.html'
