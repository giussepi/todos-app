# -*- coding: utf-8 -*-
""" spa's context preocessors """

from django.conf import settings


def optimized_backbone_app(request):
    """  """
    USE_OPTIMIZED_BACKBONE_APP = getattr(
        settings, 'USE_OPTIMIZED_BACKBONE_APP', False)
    return {'USE_OPTIMIZED_BACKBONE_APP': USE_OPTIMIZED_BACKBONE_APP}
