# -*- coding: utf-8 -*-
""" api's urls """

from django.conf.urls import patterns, include, url
from tastypie.api import Api

from .resources import TodoResource


v1_api = Api(api_name='v1')
v1_api.register(TodoResource())

urlpatterns = patterns(
    '',
    url(r'^', include(v1_api.urls)),
)
