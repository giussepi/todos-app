# -*- coding: utf-8 -*-
""" api's resources """

from tastypie.authorization import Authorization
from tastypie.resources import ModelResource

from spa.models import Todo


class TodoResource(ModelResource):
    class Meta:
        queryset = Todo.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete', 'patch']
        authorization = Authorization()
        always_return_data = True
