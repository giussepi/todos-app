#!/usr/bin/env python

import subprocess

print "Optimizing backbone app into just one file..."
try:
    subprocess.check_call('java -classpath static/js.jar:static/compiler.jar org.mozilla.javascript.tools.shell.Main static/r.js -o static/build.js', shell=True)
except subprocess.CalledProcessError:
    pass
else:
    print "Optimized main.js file created successfully."
