define(
  [
    'jquery',
    'underscore',
    'backbone',
    'collections/todos',
    'common'
  ],
  function($, _, Backbone, Todos, Common) {

    var TodoRouter = Backbone.Router.extend({
      routes:{
        '*filter': 'setFilter'
      },

      setFilter: function( param ) {
        // Set the current filter to be used
        if (param) {
          param = param.trim();
        }
        Common.TodoFilter = param || '';

        // Trigger a collection filter event, causing hiding/unhiding
        // of Todo view items
        Todos.trigger('filter');
      }
    });

    return TodoRouter;
    
  }
);

// app.TodoRouter = new Workspace();
// Backbone.history.start();
