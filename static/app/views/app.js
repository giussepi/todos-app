// var app = app || {};
// var ENTER_KEY = 13;

define(
  [
    'jquery',
    'underscore',
    'backbone',
    // collections
    'collections/todos',
    // views
    'views/todos',
    // templates
    'text!templates/stats.html',
    'common'
  ],
  function($, _, Backbone, Todos, TodoView, statsTemplate, Common) {
    // Our overall **AppView** is the top-level piece of UI.
    var AppView = Backbone.View.extend({

      // Instead of generating a new element, bind to the existing skeleton of
      // the App already present in the HTML.
      el: '#todoapp',

      // Our template for the line of statistics at the bottom of the app.
      statsTemplate: _.template(statsTemplate),

      events: {
        'keypress #new-todo': 'createOnEnter',
        'click #clear-completed': 'clearCompleted',
        'click #toggle-all': 'toggleAllComplete'
      },

      // At initialization we bind to the relevant events on the `Todos`
      // collection, when items are added or changed.
      initialize: function() {
        this.allCheckbox = this.$('#toggle-all')[0];
        this.$input = this.$('#new-todo');
        this.$footer = this.$('#footer');
        this.$main = this.$('#main');

        this.listenTo(Todos, 'add', this.addOne);
        this.listenTo(Todos, 'reset', this.addAll);

        this.listenTo(Todos, 'change:completed', this.filterOne);
        this.listenTo(Todos, 'filter', this.filterAll);
        this.listenTo(Todos, 'all', this.render);

        Todos.fetch();
      },

      // Re-rendering the App just means refreshing the statistics -- the rest
      // of the app doesn't change.
      render: function() {
        var completed = Todos.completed().length;
        var remaining = Todos.remaining().length;

        if ( Todos.length ) {
          this.$main.show();
          this.$footer.show();

          this.$footer.html(this.statsTemplate({
            completed: completed,
            remaining: remaining
          }));

          this.$('#filters li a')
            .removeClass('selected')
            .filter('[href="#/' + ( Common.TodoFilter || '' ) + '"]')
            .addClass('selected');
        } else {
          this.$main.hide();
          this.$footer.hide();
        }

        this.allCheckbox.checked = !remaining;
      },

      // Add a single todo item to the list by creating a view for it, and
      // appending its element to the `<ul>`.
      addOne: function( todo ) {
        var view = new TodoView({ model: todo });
        $('#todo-list').append( view.render().el );
      },

      // Add all items in the **Todos** collection at once.
      addAll: function() {
        this.$('#todo-list').html('');
        Todos.each(this.addOne, this);
      },

      // New
      filterOne : function (todo) {
        todo.trigger('visible');
      },

      // New
      filterAll : function () {
        Todos.each(this.filterOne, this);
      },


      // New
      // Generate the attributes for a new Todo item.
      newAttributes: function() {
        return {
          title: this.$input.val().trim(),
          order: Todos.nextOrder(),
          completed: false
        };
      },

      // New
      // If you hit return in the main input field, create new Todo model,
      // persisting it to localStorage.
      createOnEnter: function( event ) {
        if ( event.which !== Common.ENTER_KEY || !this.$input.val().trim() ) {
          return;
        }

        // seems that on localstorage the add event is triggered one extra
        // time as depicted here
        // http://stackoverflow.com/questions/10228267/backbone-js-fires-render-twice-on-collection-add
        // so to overcome this issue the new objetc is created silently 
        // and the newTodo is fired manually
        // var newTodo = app.Todos.create( this.newAttributes(), {silent: true} );
        // this.addOne(newTodo);
        
        // now that we're not using lcoalstorage there's no problem using the create
        // method
        Todos.create( this.newAttributes());

        this.$input.val('');
      },

      // New
      // Clear all completed todo items, destroying their models.
      clearCompleted: function() {
        _.invoke(Todos.completed(), 'destroy');
        return false;
      },

      // New
      toggleAllComplete: function() {
        var completed = this.allCheckbox.checked;

        Todos.each(function( todo ) {
          todo.save({
            'completed': completed
          });
        });
      }

    });

    return AppView;
    
  }
);
