define(
  [
    'underscore', 'backbone'
  ],
  function(_, Backbone) {

    var Todo = Backbone.Model.extend({

      urlRoot: '/api/v1/todo/',

      // Default attributes ensure that each todo created has `title` and
      // `completed` keys.
      defaults: {
        title: '',
        completed: false
      },

      // Toggle the `completed` state of this todo item.
      toggle: function() {
        this.save({
          completed: !this.get('completed')
        });
      }

    });

    return Todo

  }
);
