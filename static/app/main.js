require.config({
  paths: {
    'jquery': '../assets/libs/jquery-1.11.0.min',
    'backbone': '../assets/libs/backbone-min',
    'underscore': '../assets/libs/underscore-min',
    'text': '../assets/libs/text',
    'tastypie': '../assets/libs/backbone-tastypie'
  },
  shim: {
    'underscore': {
      exports: '_'
    },
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'tastypie': ['backbone', 'underscore']
  }
});

require(
  [
    'backbone',
    // views
    'views/app',
    // routers
    'routers/router',
    // plugins
    'tastypie'
  ],
  function(Backbone, AppView, Workspace) {

    new Workspace();
    Backbone.history.start();

    new AppView();

  }
);
