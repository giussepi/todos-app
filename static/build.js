({
  baseUrl: 'app',
  mainConfigFile: 'app/main.js',
  out: 'dist/main.js',
  include: ['../assets/libs/almond', 'main'],
  wrap: true,
  // wrapShim: true,
  // findNestedDependencies: true,
})
